/**
 * Gulp boilerplate
 *
 * Requirements:
 *   - NodeJS (https://nodejs.org/)
 *   - Gulp (http://gulpjs.com/)
 *
 * Start new project:
 * ```
 *     mkdir project-name
 *     cd project-name
 *     npm init
 *     npm install --save-dev gulp
 * ```
 *
 * Install each plugin in project folder with:
 * ```
 *     npm install --save-dev {plugin-name}
 * ```
 *
 * Configured plugin:
 *   - del
 *   - gulp-rename
 *   - gulp-html-minify
 *   - gulp-save
 *   - gulp-imagemin
 *   - gulp-less
 *   - gulp-sass
 *   - gulp-clean-css
 *   - gulp-concat
 *   - gulp-uglify
 */

 // https://www.npmjs.com/package/gulp-css-url-adjuster

const gulp = require('gulp');
const rename = require("gulp-rename");
const htmlminify = require('gulp-html-minify'); // Minify HTML.
const del = require('del');                     // Clean production folder.
const imagemin = require('gulp-imagemin');      // Optimize images.
const less = require('gulp-less');              // Compile LESS files from /less into /css
const sass = require('gulp-sass');              // Compile Sass into standard CSS.
const cleanCSS = require('gulp-clean-css');     // Minify compiled CSS
const concat = require('gulp-concat');          // Concat and minify javascripts.
const uglify = require('gulp-uglify');          // Concat and minify javascripts.
const mustache = require("gulp-mustache");
const browserSync = require('browser-sync').create();
const svgo = require('gulp-svgo');
const contentInclude = require('gulp-content-includer');
const fs = require('fs');

// Destination folders: EDIT THIS AS YOU LIKE.
const DIST = 'dist';

// Clean production folder.
gulp.task('clean', function(cb) {
    del.sync(DIST);
    cb();
});

// Minify HTML.
gulp.task('html' , function(cb) {
    const data = JSON.parse(fs.readFileSync('./data.json'));

    gulp.src('src/*.html')
        // .pipe(htmlminify())
        .pipe(contentInclude({
            includerReg:/<!\-\-\s*include\("([^"]+)"\)\s*\-\->/g
        }))
        .pipe(mustache({
            page: data,
        }))
        .pipe(gulp.dest(DIST))
        .on('end', cb);
});

// Copy html files to production folder.
gulp.task('copy', function(cb) {
    gulp.src('src/*.html')
        .pipe(gulp.dest(DIST))
        .on('end', cb);
})

// Optimize images.
gulp.task('imagemin', function(cb) {
    gulp.src('src/images/**/*.*')
        .pipe(svgo())
        .pipe(gulp.dest(DIST + '/images'))
        .on('end', cb);
});

// Optimize images.
gulp.task('fonts', function(cb) {
    gulp.src('src/fonts/*')
        .pipe(gulp.dest(DIST + '/fonts'))
        .on('end', cb);
});

// Compile Sass into standard CSS.
gulp.task('sass', function(cb) {
    gulp.src('src/sass/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('src/css'))
        .on('end', cb);
});

// Compile LESS files from /less into /css
gulp.task('less', function(cb) {
    gulp.src('src/less/*.less')
        .pipe(less())
        .pipe(gulp.dest('/css'))
        .on('end', cb);
});

// Concat and minify javascripts.
// Destination file name: 'main.min.js'
gulp.task('js', function(cb) {
    gulp.src([
            'src/js/jquery-3.3.1.min.js', // todo: solve this shit
            'src/js/swiper.min.js', // todo: solve this shit
            'src/js/main.js'
        ])
        .pipe(concat('main.js'))
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest(DIST + '/js'))
        .on('end', cb);
});

// Minify compiled CSS (destination file name: 'style.min.css')
gulp.task('css', function(cb) {
    gulp.src('src/css/*.css')
        .pipe(concat('style.css'))
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest(DIST + '/css'))
        .on('end', cb);
});

gulp.task('vendors-css', function(cb) {
    gulp.src('src/vendors/css/*.css')
        .pipe(gulp.dest('src/css'))
        .on('end', cb);
});

gulp.task('vendors-js', function(cb) {
    gulp.src('src/vendors/js/*.js')
        .pipe(gulp.dest('src/js'))
        .on('end', cb);
});

gulp.task('vendors', ['vendors-js', 'vendors-css'], function (cb) {
    cb();
});

gulp.task('browser-sync', function() {
    browserSync.init({
        server: {
            baseDir: './dist'
        },
        notify: false,
    });
});

// Run all tasks.
gulp.task('default', ['clean', 'html', 'imagemin', 'vendors', 'less', 'sass', 'css', 'js', 'fonts']);

// Watch for changes.
gulp.task('dev', ['browser-sync', 'default'], function() {
    gulp.watch('src/fonts/*.*', ['fonts']).on('change', browserSync.reload)
    gulp.watch('src/js/*.js', ['js']).on('change', browserSync.reload)
    gulp.watch('src/css/*.css', ['css']).on('change', browserSync.reload)
    gulp.watch('src/sass/*.scss', ['sass', 'css']).on('change', browserSync.reload)
    gulp.watch('src/less/*.less', ['less', 'css']).on('change', browserSync.reload)
    gulp.watch('src/images/**/*.*', ['imagemin']).on('change', browserSync.reload)
    gulp.watch('src/inc/*.html', ['html']).on('change', browserSync.reload)
    gulp.watch('src/*.html', ['html']).on('change', browserSync.reload)
    gulp.watch('src/vendors/*/*', ['vendors']).on('change', browserSync.reload)
    gulp.watch('./*.json', ['html']).on('change', browserSync.reload)
});