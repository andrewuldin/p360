(function(b,c){var $=b.jQuery||b.Cowboy||(b.Cowboy={}),a;$.throttle=a=function(e,f,j,i){var h,d=0;if(typeof f!=="boolean"){i=j;j=f;f=c}function g(){var o=this,m=+new Date()-d,n=arguments;function l(){d=+new Date();j.apply(o,n)}function k(){h=c}if(i&&!h){l()}h&&clearTimeout(h);if(i===c&&m>e){l()}else{if(f!==true){h=setTimeout(i?k:l,i===c?e-m:e)}}}if($.guid){g.guid=j.guid=j.guid||$.guid++}return g};$.debounce=function(d,e,f){return f===c?a(d,e,false):a(d,f,e!==false)}})(this);
!function(t,e){"use strict";"initCustomEvent"in e.createEvent("CustomEvent")&&(t.CustomEvent=function(t,n){n=n||{bubbles:!1,cancelable:!1,detail:void 0};var u=e.createEvent("CustomEvent");return u.initCustomEvent(t,n.bubbles,n.cancelable,n.detail),u},t.CustomEvent.prototype=t.Event.prototype),e.addEventListener("touchstart",function(t){if("true"===t.target.getAttribute("data-swipe-ignore"))return;s=t.target,l=Date.now(),n=t.touches[0].clientX,u=t.touches[0].clientY,a=0,i=0},!1),e.addEventListener("touchmove",function(t){if(!n||!u)return;var e=t.touches[0].clientX,l=t.touches[0].clientY;a=n-e,i=u-l},!1),e.addEventListener("touchend",function(t){if(s!==t.target)return;var e=parseInt(s.getAttribute("data-swipe-threshold")||"20",10),o=parseInt(s.getAttribute("data-swipe-timeout")||"500",10),r=Date.now()-l,c="";Math.abs(a)>Math.abs(i)?Math.abs(a)>e&&r<o&&(c=a>0?"swiped-left":"swiped-right"):Math.abs(i)>e&&r<o&&(c=i>0?"swiped-up":"swiped-down");""!==c&&s.dispatchEvent(new CustomEvent(c,{bubbles:!0,cancelable:!0}));n=null,u=null,l=null},!1);var n=null,u=null,a=null,i=null,l=null,s=null}(window,document);

function is_touch_device() {
    var prefixes = ' -webkit- -moz- -o- -ms- '.split(' ');
    var mq = function(query) {
    return window.matchMedia(query).matches;
    }

    if (('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch) {
    return true;
    }

    // include the 'heartz' as a way to have a non matching MQ to help terminate the join
    // https://git.io/vznFH
    var query = ['(', prefixes.join('touch-enabled),('), 'heartz', ')'].join('');
    return mq(query);
}

$().ready(function() {
    var mySwiper;

    function manageSizes() {
        var vh = $(window).outerHeight();
        var mobile = is_touch_device() && $(window).outerWidth() < 650;
        $('.swiper-container').css({height: vh - (mobile ? 90 : 120)});
        $('.swiper__image').css({height: vh - (mobile ? 90 : 120)});
        $('.wrapper').css({height: vh});
    }
    manageSizes();

    function fillSlider() {
        if (mySwiper) {
            mySwiper.destroy(true, true);
            $('.swiper-wrapper').html('');
        }
        manageSizes();
        mySwiper = new Swiper('.swiper-container', {
            loop: true,
            init: false,

            // If we need pagination
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
            },
            autoplay: {
                delay: 4000,
            }
        });

        if (is_touch_device() && $(window).outerWidth() < 650) {
            $('.swiper-slides.mobile > .swiper-slide').clone().each(function(i, el) {
                mySwiper.appendSlide(el)
            });
        } else {
            $('.swiper-slides.desktop > .swiper-slide').clone().each(function(i, el) {
                mySwiper.appendSlide(el)
            });
        }
        mySwiper.init();
    }
    fillSlider();

    $(window).on('resize', $.throttle(500, fillSlider));

    $(document).on('swiped-up', function(e) {
        $('body').addClass('swipped-up');
    });
    $(document).on('swiped-down', function(e) {
        $('body').removeClass('swipped-up');
    });

    $('.subscription').on('keyup', 'input', function(e) {
        var email = $(e.target).val();
        if (validateEmail(email)){
            $('.subscription').addClass('subscription_valid_yes');
        } else {
            $('.subscription').removeClass('subscription_valid_yes');
        }
    });

    function setSubscriptionSuccess() {
        $('.subscription').addClass('subscription_sent_yes');
    }

    function setSubscriptionError() {
        $('.subscription').addClass('subscription_error_yes');
    }

    function clearSubscriptionState() {
        $('.subscription').removeClass('subscription_sent_yes');
        $('.subscription').removeClass('subscription_error_yes');
        $('.subscription input[type=email]').val('');
    }

    function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    $('.subscription').on('submit', function(e) {
        e.preventDefault();
        var email = $('.subscription input').val();

        if (validateEmail(email)){
            setSubscriptionError();
            return;
        }

        $.ajax({
            url: e.target.action,
            type: 'GET',
            data: $('.subscription').serialize(),
            dataType: 'json',
            success: function(data) {
                if (data.success) {
                    setSubscriptionSuccess();
                    return;
                }

                setSubscriptionError();
            },
            error: function() {
                setSubscriptionError();
            }
        })
    });

    $('.subscription__success a').on('click', function(e) {
        e.preventDefault();
        clearSubscriptionState();
    });
    $('.subscription__error a').on('click', function(e) {
        e.preventDefault();
        clearSubscriptionState();
    });
});
